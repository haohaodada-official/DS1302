'use strict';

goog.provide('Blockly.Blocks.colour');
goog.require('Blockly.Blocks');
goog.require('Blockly.constants');

var sensor_colorSet = '#515070';

if (!Blockly.Blocks['pins_digital']) {
  var pin_digital = [["0", "0"], ["1", "1"], ["2", "2"], ["3", "3"], ["4", "4"], ["5", "5"], ["6", "6"], ["7", "7"], ["8", "8"], ["9", "9"], ["10", "10"], ["11", "11"], ["12", "12"], ["13", "13"], ["14", "14"], ["15", "15"], ["16", "16"], ["17", "17"], ["18", "18"], ["19", "19"], ["20", "20"], ["21", "21"], ["22", "22"], ["23", "23"], ["24", "24"], ["25", "25"], ["26", "26"], ["27", "27"], ["28", "28"], ["29", "29"], ["30", "30"], ["31", "31"], ["32", "32"], ["33", "33"], ["34", "34"], ["35", "35"], ["36", "36"], ["37", "37"], ["38", "38"], ["39", "39"], ["40", "40"], ["41", "41"], ["42", "42"], ["43", "43"], ["44", "44"], ["45", "45"], ["46", "46"], ["47", "47"], ["48", "48"], ["49", "49"], ["50", "50"], ["51", "51"], ["52", "52"], ["53", "53"], ["A0", "A0"], ["A1", "A1"], ["A2", "A2"], ["A3", "A3"], ["A4", "A4"], ["A5", "A5"], ["A6", "A6"], ["A7", "A7"], ["A8", "A8"], ["A9", "A9"], ["A10", "A10"], ["A11", "A11"], ["A12", "A12"], ["A13", "A13"], ["A14", "A14"], ["A15", "A15"]];
  Blockly.Blocks['pins_digital'] = {
    init: function () {
      this.setColour(230);
      this.appendDummyInput("")
        .appendField(new Blockly.FieldDropdown(pin_digital), 'PIN');
      this.setOutput(true, ["Number", Number]);
    }
  };
}

/*RTC*/
Blockly.Blocks.DS1302_init = {
  init: function () {
    this.setColour(sensor_colorSet);
    this.appendDummyInput("")
      .setAlign(Blockly.ALIGN_RIGHT)
      .appendField("初始化时钟模块DS1302 管脚 #");
    //.appendField(new Blockly.FieldTextInput('myRTC'), 'RTCName');
    this.appendValueInput("RST", ["Number", Number])
      .appendField("RST#")
      .setCheck(["Number", Number]);
    this.appendValueInput("DAT")
      .appendField("DAT#")
      .setCheck(["Number", Number]);
    this.appendValueInput("CLK")
      .appendField("CLK#")
      .setCheck(["Number", Number]);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setInputsInline(true);
    this.setTooltip("初始化DS1302 RTC时钟模块，参数为重置线管脚号，数据线管脚号和时钟线管脚号");
  }
};

//DS1307 RTC
Blockly.Blocks.DS1307_init = {
  init: function () {
    this.setColour(sensor_colorSet);
    this.appendDummyInput("")
      .setAlign(Blockly.ALIGN_RIGHT)
      .appendField("初始化时钟模块DS1307 管脚 #");
    //.appendField(new Blockly.FieldTextInput('myRTC'), 'RTCName');
    this.appendValueInput("SDA")
      .appendField("SDA#")
      .setCheck(["Number", Number]);
    this.appendValueInput("SCL")
      .appendField("SCL#")
      .setCheck(["Number", Number]);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setInputsInline(true);
    this.setTooltip("初始化DS1307 RTC时钟模块，参数为数据线管脚号和时钟线管脚号");
  }
};
//传感器-实时时钟块_时间变量
Blockly.YEAR = '年';
Blockly.MONTH = '月';
Blockly.DAY = '日';
Blockly.HOUR = '时';
Blockly.MINUTE = '分';
Blockly.SECOND = '秒';
Blockly.WEEK = '周';
var RTC_TIME_TYPE = [
  [Blockly.YEAR, "Year"],
  [Blockly.MONTH, "Month"],
  [Blockly.DAY, "Day"],
  [Blockly.HOUR, "Hour"],
  [Blockly.MINUTE, "Minute"],
  [Blockly.SECOND, "Second"],
  [Blockly.WEEK, "DOW"]
];
//传感器-实时时钟块_获取时间
Blockly.Blocks.RTC_get_time = {
  init: function () {
    this.setColour(sensor_colorSet);
    this.appendDummyInput("")
      .setAlign(Blockly.ALIGN_RIGHT)
      .appendField("RTC获取时间");
    this.appendDummyInput("")
      .setAlign(Blockly.ALIGN_RIGHT);
    //.appendField(new Blockly.FieldTextInput('myRTC'), 'RTCName');
    this.appendDummyInput("")
      .setAlign(Blockly.ALIGN_RIGHT)
      .appendField(new Blockly.FieldDropdown(RTC_TIME_TYPE), "TIME_TYPE");
    this.setInputsInline(true);
    this.setOutput(true, ["Number", Number]);
    this.setTooltip("从RTC时钟模块获取%1".replace('%1', this.getFieldValue("TIME_TYPE")));
  }
};
// //传感器-实时时钟块_设置时间
Blockly.Blocks.RTC_time = {
  init: function () {
    this.setColour(sensor_colorSet);
    this.appendValueInput("hour")
      .setCheck(["Number", Number]);
    this.appendDummyInput("")
      .appendField(Blockly.HOUR);
    this.appendValueInput("minute")
      .setCheck(["Number", Number]);
    this.appendDummyInput("")
      .appendField(Blockly.MINUTE);
    this.appendValueInput("second")
      .setCheck(["Number", Number]);
    this.appendDummyInput("")
      .appendField(Blockly.SECOND);
    this.setInputsInline(true);
    this.setOutput(true, null);
    this.setTooltip("设置时间");
  }
};

Blockly.Blocks.RTC_date = {
  init: function () {
    this.setColour(sensor_colorSet);
    this.appendValueInput("year")
      .setCheck(["Number", Number]);
    this.appendDummyInput("")
      .appendField(Blockly.YEAR);
    this.appendValueInput("month")
      .setCheck(["Number", Number]);
    this.appendDummyInput("")
      .appendField(Blockly.MONTH);
    this.appendValueInput("day")
      .setCheck(["Number", Number]);
    this.appendDummyInput("")
      .appendField(Blockly.DAY);
    this.setInputsInline(true);
    this.setOutput(true, null);
    this.setTooltip("设置日期");
  }
};
//设置时间
Blockly.Blocks.RTC_set_time = {
  init: function () {
    this.appendDummyInput()
      .setAlign(Blockly.ALIGN_RIGHT)
      .appendField("RTC设置时间");
    this.appendValueInput("date")
      .appendField("日期");
    this.appendValueInput("time")
      .appendField("时间");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(sensor_colorSet);
    this.setTooltip("");
    this.setHelpUrl("");
  }
};
//获取烧录时间和日期
Blockly.Blocks.get_system_date_time = {
  init: function () {
    this.appendDummyInput()
      .appendField("获取 系统")
      .appendField(new Blockly.FieldDropdown([["日期", "DATE"], ["时间", "TIME"]]), "type");
    this.setInputsInline(false);
    this.setOutput(true, null);
    this.setColour(40);
    this.setTooltip("");
    this.setHelpUrl("");
  }
};
//传感器-实时时钟块_设置日期
Blockly.Blocks.RTC_set_date = {
  init: function () {
    this.setColour(sensor_colorSet);
    this.appendDummyInput("")
      .setAlign(Blockly.ALIGN_RIGHT)
      .appendField("设置日期");
    // .appendField(new Blockly.FieldTextInput('myRTC'), 'RTCName');
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip("设置日期");
  }
};