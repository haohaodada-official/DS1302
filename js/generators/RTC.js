'use strict';

goog.provide('Blockly.Arduino.Base');
goog.require('Blockly.Arduino');

if (!Blockly.Arduino.pins_digital) {
  Blockly.Arduino.pins_digital = function () {
    var code = this.getFieldValue('PIN');
    return [code, Blockly.Arduino.ORDER_ATOMIC];
  };
}

/*RTC*/
Blockly.Arduino.DS1302_init = function () {
  var dropdown_rst = Blockly.Arduino.valueToCode(this, 'RST', Blockly.Arduino.ORDER_ATOMIC);
  var dropdown_dat = Blockly.Arduino.valueToCode(this, 'DAT', Blockly.Arduino.ORDER_ATOMIC);
  var dropdown_clk = Blockly.Arduino.valueToCode(this, 'CLK', Blockly.Arduino.ORDER_ATOMIC);
  var RTCName = "myRTC";
  Blockly.Arduino.definitions_['include_DS1302'] = '#include <DS1302.h>';
  Blockly.Arduino.definitions_['var_declare_' + RTCName] = 'DS1302 ' + RTCName + '(' + dropdown_rst + ',' + dropdown_dat + ',' + dropdown_clk + ');';
  return "";
};

Blockly.Arduino.DS1307_init = function () {
  var SDA = Blockly.Arduino.valueToCode(this, 'SDA', Blockly.Arduino.ORDER_ATOMIC);
  var SCL = Blockly.Arduino.valueToCode(this, 'SCL', Blockly.Arduino.ORDER_ATOMIC);
  var RTCName = "myRTC";
  Blockly.Arduino.definitions_['include_DS1307'] = '#include <DS1307.h>';
  Blockly.Arduino.definitions_['var_declare_DS1307'] = 'DS1307 ' + RTCName + '(' + SDA + ',' + SCL + ');';
  Blockly.Arduino.setups_[RTCName + '_begin'] = RTCName + '.begin();';
  return "";
}

Blockly.Arduino.RTC_get_time = function () {
  var timeType = this.getFieldValue('TIME_TYPE');
  var RTCName = "myRTC";
  var code = RTCName + '.get' + timeType + '()';
  return [code, Blockly.Arduino.ORDER_ATOMIC];
}

Blockly.Arduino.RTC_date = function () {
  var year = Blockly.Arduino.valueToCode(this, "year", Blockly.Arduino.ORDER_ATOMIC);
  var month = Blockly.Arduino.valueToCode(this, "month", Blockly.Arduino.ORDER_ATOMIC);
  var day = Blockly.Arduino.valueToCode(this, "day", Blockly.Arduino.ORDER_ATOMIC);
  var code = '"' + year + '/' + month + '/' + day + '"';
  return [code, Blockly.Arduino.ORDER_ATOMIC];
}

Blockly.Arduino.RTC_time = function () {
  var hour = Blockly.Arduino.valueToCode(this, "hour", Blockly.Arduino.ORDER_ATOMIC);
  var minute = Blockly.Arduino.valueToCode(this, "minute", Blockly.Arduino.ORDER_ATOMIC);
  var second = Blockly.Arduino.valueToCode(this, "second", Blockly.Arduino.ORDER_ATOMIC);
  var code = '"' + hour + ':' + minute + ':' + second + '"';
  return [code, Blockly.Arduino.ORDER_ATOMIC];
}

//设置时间
Blockly.Arduino.RTC_set_time = function () {
  var value_date = Blockly.Arduino.valueToCode(this, 'date', Blockly.Arduino.ORDER_ATOMIC);
  var value_time = Blockly.Arduino.valueToCode(this, 'time', Blockly.Arduino.ORDER_ATOMIC);
  value_date = value_date.replace(/\//g, ',').replace(/"/g, '');
  value_time = value_time.replace(/:/g, ',').replace(/"/g, '');
  var code = '';
  var RTCName = "myRTC";
  var code = RTCName + '.setDate(' + value_date + ');\n';
  code += RTCName + '.setDOW(' + value_date + ');\n';
  code += RTCName + '.setTime(' + value_time + ');\n';
  return code;
};
//获取烧录时间和日期
Blockly.Arduino.get_system_date_time = function () {
  var dropdown_type = this.getFieldValue('type');
  var code = '__' + dropdown_type + '__';
  return [code, Blockly.Arduino.ORDER_ATOMIC];
};
Blockly.Arduino.RTC_set_date = function () {
  var RTCName = "myRTC";
  var code = RTCName + '.setDate(' + year + ',' + month + ',' + day + ');\n';
  code += RTCName + '.setDOW(' + year + ',' + month + ',' + day + ');\n';
  return code;
};